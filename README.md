<div align="center"><h1><ins>DevOps Demo Project:</ins></h1></div>
<div align="center"><h2>Ansible Integration in Jenkins</h2></div>

<div align="center">![1](img/devOps.png)</div>
<br>

<ins>DevOps Tools used in this project</ins>:<br>- Ansible<br>- AWS EC2<br>- Digital Ocean<br>- Jenkins<br>- Apache Groovy<br>- Git<br>- GitLab<br>- Microsoft Visual Studio Code<br>- Python: pip3, boto3, & botocore<br>- Windows PowerShell

<ins>Operating Systems:</ins><br>- Linux<br>- Windows 11

<ins>Project Purpose</ins>: Deploy a Jenkins CI/CD pipeline with stages to copy files to an Ansible server and execute an Ansible playbook

<ins>Out of scope</ins>:<br>- Containerized installation of Jenkins<br>- Digital Ocean account creation<br>- AWS account creation<br>- Git installation on Windows 11<br>- GitLab account creation<br>- SSH key creation in Digital Ocean<br>- AWS IAM Access Key creation<br>- Adding GitLab Credentials to Jenkins

# Project Walkthrough
I. [GitLab Project Creation](#project)<br>
II. [CI/CD Stage: Copy Files to Ansible Server](#copy_files)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A) [Prerequisites:](#prereq)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i) [Ansible Control Node Prep](#ansible_control_node)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a) [Create/SSH into Digital Ocean Droplet](#droplet)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b) [Ansible Install](#ansible_install)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c) [Python3: pip3, boto3, & botocore](#python)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;d) [AWS Credentials on Ansible Server](#aws_creds)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ii) [AWS: EC2 Instance & Key Pair Creation](#aws_ec2_keypair)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iii) [Ansible Configuration Files](#ansible_config)<br> 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a) [ansible.cfg and Dynamic Inventory File](#dyn_inv)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b) [Ansible Playbook](#playbook)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iv) [Jenkins Prerequisites:](#jprereq)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a) [Plugins: SSH Agent/Credentials Binding](#plugins1)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b) [Ansible Server Key Credentials](#ansible_server_key)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c) [EC2 Server Key Credentials](#ec2_server_key)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B) [Create Jenkinsfile](#jf1)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C) [Jenkins Pipeline Creation/Execution](#pipeline1)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D) [Pipeline Validation & Security Optimization](#optimize1)<br>
III. [CI/CD Stage: Execute Ansible Playbook](#execute_playbook)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A) [Jenkins Prereq: SSH Pipeline Steps Plugin](#plugins2)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B) [Jenkinsfile](#jf2)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C) [Jenkins Pipeline Execution](#pipeline_execute)<br>
IV. [Jenkins Pipeline Optimization](#optimize2)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A) [Environment Variable: Ansible Server IP](#env_var)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B) [Bash Script: Prepare-Ansible-Server.sh](#bash)<br>
V. [Project Teardown](#teardown)


## I. GitLab Project Creation <a name="project"></a>
The purpose of this section to create a local project in Microsoft Visual Studio Code and push the project to the remote GitLab repository.
1.	Open File Explorer. Navigate to the root of C:\ and create a folder called ansible-jenkins.

    ![1pc](img/project_creation/1.png) 

2.	Open Microsoft Visual Studio Code. Click on the File menu > Open Folder...

    ![2pc](img/project_creation/2.png) 
 
3.	Browse out to the root of C:\ and select the ansible-jenkins folder. Click the <b>Select Folder</b> button.
 
    ![3pc](img/project_creation/3.png) 

4.	When prompted, click the <b>Yes, I trust the authors</b> button.

    ![4pc](img/project_creation/4.png)  

5.	The project folder will now appear in the left pane.

    ![5pc](img/project_creation/5.png)  

6.	Under the root of the project folder, create a blank README.md file.
 
    ![6pc](img/project_creation/6.png) 

7.	Minimize Microsoft Visual Studio Code. Open a web browser and login to GitLab. Click the <b>New Project</b> button.

    ![7pc](img/project_creation/7.png)  

8.	On the Create new project screen, click Create blank project.
 
    ![8pc](img/project_creation/8.png) 

9.	On the Create blank project screen, name the project “ansible-jenkins.” In the Project URL section, specify the username from the dropdown (e.g. jlomba09), and ensure the project slug matches the project name. Set the visibility level to Public and uncheck the box to initialize the repository with a README. Click the <b>Create project</b> button.

    ![9pc](img/project_creation/9.png)  

10.	Maximize Microsoft Visual Studio Code. In the top left menu, click the Ellipsis (...)  > Terminal > New Terminal.

    ![10pc](img/project_creation/10.png)  

11.	<ins>Note</ins>: Although the installation of Git on Windows 11 is outside the scope of this project walkthrough, the Git installer can be found here: https://git-scm.com/download/win

    In the bottom terminal pane, initialize the git repository with the following command:

        git init

    The output will show the repository is initialized.

    ![11pc](img/project_creation/11.png)  

12.	Set the origin as the newly created project in GitLab. Substitute the username (e.g. jlomba09) with the appropriate username.

        git remote add origin git@gitlab.com:username/ansible-jenkins.git

13.	Add changes and set the initial commit message:

        git add .
        git commit -m "Initial commit"

14.	Set the push behavior to the master branch of the GitLab repository with the following command:

        git push -u origin master

15.	Restore the web browser, refresh the ansible-jenkins project in GitLab, and confirm the blank README file exists:

    ![12pc](img/project_creation/12.png)  

New files can now be added to the GitLab project.

## II. CI/CD Stage: Copy Files to Ansible Server <a name="copy_files"></a>
Prior to writing the "copy files to Ansible server" stage in Jenkinsfile, a number of prerequisites must be met. First, the Ansible Control Node will be prepared using the Digital Ocean cloud platform. Second, two AWS EC2 instances and their associated key pair will be created. Then, the Ansible configuration files will be created. Next, plugins will be installed inside the Jenkins management UI. Last, the EC2 and Ansible credentials will be added to the Jenkins credentials store. 

After the prerequisites are satisfied, the "copy files to Ansible server" stage will be written in Jenkinsfile, and the pipeline in Jenkins will be created and executed. This section will conclude with validating the Jenkins pipeline and with a security optimization.
## A) Prerequisites: <a name="prereq"></a>
This section outlines the prerequisites required prior to writing the "copy files to Ansible server" stage in Jenkinsfile. First, a Digital Ocean droplet will be created  where Ansible and Python-related packages will be installed. AWS credentials will also be added to the Ansible server for programmatic access. Next, two EC2 instances and their associated key pair will be created in AWS. Then, Ansible configuration files will be created. Last, Jenkins plugins and EC2/Ansible server key credentials will be added to the Jenkins credentials store. Each of these prerequisites will be addressed next.
## i) Ansible Control Node Prep <a name="ansible_control_node"></a>
Preparing the Ansible Control Node is comprised of three parts. First, a Digital Ocean droplet will be created. Then, Ansible and Python3-related packages will be installed. Sections b) and c) highlight the manual installation of Ansible and Python3-related packages, respectively. The final preparation step for the Ansible Control Node will be to add AWS credentials to the Ansible server. 

## a) Create/SSH into Digital Ocean Droplet<a name="droplet"></a>
Rather than install Ansible on the existing Jenkins container, a dedicated Ansible server will be created to adhere to the best practice of separating server roles. This way, if something happens to the Ansible server, it will not have an impact on Jenkins functionality and vice versa. This section will conclude with establishing an SSH session into the newly created Digital Ocean-hosted Ansible server.
1. Login to Digital Ocean. In the left navigation menu, click Droplets. 

    ![1cfpapd](img/copy_files/prereq/ansible_prep/droplet/1.png)  

2. On the Droplets screen, click the <b>Create Droplet</b> button.

    ![2cfpapd](img/copy_files/prereq/ansible_prep/droplet/2.png)  

3. On the Create Droplets screen, select the nearest region and datacenter. This project walkthrough assumes New York Datacenter 1. 
 
    ![3cfpapd](img/copy_files/prereq/ansible_prep/droplet/3.png)

4. In the Choose an image section, select the latest version of Ubuntu. 
 
    ![4cfpapd](img/copy_files/prereq/ansible_prep/droplet/4.png)

5. In the Choose Size section, select Basic as the droplet type with a regular SSD. Select the 2GB RAM/2 CPUs/60 GB SSD/3 TB transfer option. 

    ![5cfpapd](img/copy_files/prereq/ansible_prep/droplet/5.png) 

6. In the Choose Authentication Method section, select the SSH Key radio button. 

    ![6cfpapd](img/copy_files/prereq/ansible_prep/droplet/6.png) 

7. Click the <b>Create Droplet</b> button at the bottom.

    ![7cfpapd](img/copy_files/prereq/ansible_prep/droplet/7.png)
 
8. On the first-project screen, click on the newly created droplet. 

    ![8cfpapd](img/copy_files/prereq/ansible_prep/droplet/8.png) 

9. Click on the name of the server and a text box will appear to allow editing of the server name. Name the server as ansible-server and click the blue checkmark to save the changes.

    ![9cfpapd](img/copy_files/prereq/ansible_prep/droplet/9.png) 

10. Click on Droplets in the left navigation menu and confirm the server’s name is changed:

    ![10cfpapd](img/copy_files/prereq/ansible_prep/droplet/10.png) 

11. Copy the IP address to the clipboard and open a PowerShell window. Inside the PowerShell window, run the SSH command to login to the Ansible server as the root user, pasting the IP address of the Ansible server from the clipboard.

    When prompted, type yes to add the server IP to the list of known hosts.

    ![1cfpapai](img/copy_files/prereq/ansible_prep/ansible_install/1.png) 

The Digital Ocean droplet is successfully created and an SSH session is established.

## b) Ansible Install <a name="ansible_install"></a>
The section will install Ansible on the Digital Ocean droplet.

1. Inside the SSH session, run the following command to update the apt repository’s package list. The sudo command is not required as the server is logged in as the root user:

        apt update

2.	Run the following command to install ansible. When prompted to continue, type Y and press Enter.

        apt install ansible

3.	Test Ansible is readily available by entering the following command:

        ansible --version

    The following output will display:
 
    ![2cfpapai](img/copy_files/prereq/ansible_prep/ansible_install/2.png) 

Ansible is now ready for use.

## c) Python3: pip3, boto3, & botocore <a name="python"></a>
The official Ansible documentation displays the following prerequisites for using any AWS-related module:

  ![1cfpapp](img/copy_files/prereq/ansible_prep/python3/1.png)  

The requirements for the amazon.aws.ec2_ami module are shown here (https://docs.ansible.com/ansible/latest/collections/amazon/aws/ec2_ami_module.html#ansible-collections-amazon-aws-ec2-ami-module), but the same requirements hold true for any other AWS-related module.

Accordingly, this section will outline the installation of the boto3 and botocore Python packages.
1.	In the SSH session of the Ansible server, run the following command to install pip3. Enter Y when prompted to continue:

        apt install python3-pip

2.	When attempting to use pip3 to install the boto3 and botocore packages, the command output will produce the following error:

    ![2cfpapp](img/copy_files/prereq/ansible_prep/python3/2.png)   

3. Per the standard error suggestion above, use the following command to install the boto3 package. When prompted, enter Y to continue:

        apt install python3-boto3

4.	Confirm the botocore package is already installed:
 
    ![3cfpapp](img/copy_files/prereq/ansible_prep/python3/3.png)   

5.	Run the following command to compile the boto3 and botocore packages. If no errors display, the requirements are met:

        python3 -c "import boto3"
        python3 -c "import botocore"

    ![4cfpapp](img/copy_files/prereq/ansible_prep/python3/4.png)    

## d) AWS Credentials on Ansible Server <a name="aws_creds"></a>
A dynamic inventory file that configures the AWS EC2 inventory plugin will later be used in the Ansible playbook. Prior to using the dynamic inventory file, configuring AWS credentials on the Ansible server is necessary. The dynamic inventory file provides two benefits: (1) adherence to best security practice of not hardcoding the EC2 IP addresses and (2) automatic retrieval of EC2 instance information in lieu of manual entry. For this process to occur, the AWS credentials will need to authenticate against the AWS account.
1.	Return to the established SSH session inside the Ansible server. Issue the following command to confirm the present working directory is the root user’s home folder:

        pwd

    The output confirms the desired location:

    ![1cfpapac](img/copy_files/prereq/ansible_prep/aws_creds/1.png)    

2.	Issue the following command to create an .aws directory inside the root user’s home folder:

        mkdir .aws

3.	Change to the .aws directory

        cd .aws

4.	Inside the .aws directory, create a file named credentials and open it inside the VIM editor. Press the “i” key to enter Insert Mode.

        vim credentials

5. <ins>Note</ins>: This project walkthrough assumes the engineer is in possession of the AWS access key and the access key is already available natively in Windows. If necessary, create a new access key in the AWS IAM management page. Be sure to save the credentials to a safe location!

    Open PowerShell and issue the following command to display the AWS credentials as terminal output:

        type .aws/credentials

    Copy the entire output to the clipboard.

    ![2cfpapac](img/copy_files/prereq/ansible_prep/aws_creds/2.png)   

6.	Inside the VIM editor, paste the contents from the clipboard. Hit the Esc key to exit Insert Mode. Enter :wq to save and quit.

    ![3cfpapac](img/copy_files/prereq/ansible_prep/aws_creds/3.png)    

7.	Close the SSH session.

        exit

Preparation of the Ansible Control Node is complete.
## ii) AWS: EC2 Instance & Key Pair Creation <a name="aws_ec2_keypair"></a>
This section will create two AWS EC2 instances and create an associated key pair.
1.	Login to the AWS Management Console.

2.	In the search bar, enter “ec2” as a search criterion. Under Services, click EC2 to navigate to the EC2 Dashboard.

    ![1cfpaekp](img/copy_files/prereq/aws_ec2_keypair/1.png)    

3.	On the EC2 Dashboard, click Instances (running).

    ![2cfpaekp](img/copy_files/prereq/aws_ec2_keypair/2.png)   
 
4.	On the Instances screen, click the orange <b>Launch instances</b> button.

    ![3cfpaekp](img/copy_files/prereq/aws_ec2_keypair/3.png)    

5.	On the Launch an instance screen, in the Application and OS Images (Amazon Machine Image) section, select Amazon Linux. From the Amazon Machine Image (AMI) dropdown, specify Amazon Linux 2 AMI (HVM) – Kernel 5.10, SSD Volume Type. Leave the default architecture as 64-bit.

    ![4cfpaekp](img/copy_files/prereq/aws_ec2_keypair/4.png)    

6. In the Instance type section, accept the default instance type of t2.micro.

    ![5cfpaekp](img/copy_files/prereq/aws_ec2_keypair/5.png)    

7.	In the Key pair (login) section, click <b>Create new key pair</b>.
 
    ![6cfpaekp](img/copy_files/prereq/aws_ec2_keypair/6.png)   

8.	In the Create key pair dialog box, specify the key name as ansible-jenkins. Leave the default key pair type as RSA. Leave the default private key file format as .pem. Click the <b>Create key pair</b> button.

    ![7cfpaekp](img/copy_files/prereq/aws_ec2_keypair/7.png)    

    The .pem file will be located in the Downloads folder.

9.	Accept the defaults for the Network settings and Configure storage sections. In the Summary section, specify the number of instances as 2. Click on the <b>Launch instance</b> button.

    ![8cfpaekp](img/copy_files/prereq/aws_ec2_keypair/8.png)    

10.	Return up a level to the Instances screen. 
 
    ![9cfpaekp](img/copy_files/prereq/aws_ec2_keypair/9.png)   

11.	Wait a moment for the instances to be in the running and initialized states.

    ![10cfpaekp](img/copy_files/prereq/aws_ec2_keypair/10.png)    

Two AWS EC2 instances are created, and both are tied to the same key pair.
## iii) Ansible Configuration Files <a name="ansible_config"></a>
This section will create an Ansible configuration file, a dynamic inventory file, and an Ansible playbook.
1.	Open the ansible-jenkins project in Microsoft Visual Studio Code. Click the icon to create a new folder. Name the folder “ansible.”

    ![1cfpacf](img/copy_files/prereq/ansible_config_files/1.png)    

2.	Create 3 new files inside the ansible folder: (1) ansible.cfg, (2) inventory_aws_ec2.yaml, and (3) my-playbook.yaml.
 
    ![2cfpacf](img/copy_files/prereq/ansible_config_files/2.png) 

The Ansible configuration file and dynamic inventory file are addressed next.
## a) ansible.cfg and Dynamic Inventory File <a name="dyn_inv"></a>
Refer to the official Ansible documentation for further reading on options available for the Ansible configuration file: https://docs.ansible.com/ansible/latest/reference_appendices/config.html 

1.	In Microsoft Visual Studio Code, open the ansible.cfg file. As the first line, create a defaults section:

        [defaults]

2.	As the second line of ansible.cfg, add the host_key_checking option and set it to False. This non-interactively bypasses the yes/no prompt for adding the server IP to the list of known hosts.

        host_key_checking = False
3.	Third, specify the inventory option with the value as the dynamic inventory file (i.e. inventory_aws_ec2.yaml).
        
        inventory = inventory_aws_ec2.yaml

4.	Enter a couple carriage returns for spacing. Specify the interpreter_python option with the path to the Python3 installation:

        interpreter_python = /usr/bin/python3

5.	Under the interpreter_python option, add an option for enable_plugins. Set the value as aws_ec2:

        enable_plugins = aws_ec2

6.	Enter a couple carriage returns for spacing. As the default user with every AWS EC2 instance is ec2-user, add the remote_user option and add ec2-user as the value:

        remote_user = ec2-user

7.	Last, specify the private_key_file option. Set the value to ~/ssh-key.pem.

        private_key_file = ~/ssh-key.pem

    <ins>Note</ins>: The .pem file is the same as the ansible-jenkins.pem file created in section ii), step 8 for the AWS EC2 server. The name “ssh-key” will be the name of the saved file when the scp command is used later in Jenkinsfile.

The ansible.cfg configuration file is complete. The dynamic inventory file (inventory_aws_ec2.yaml) will be addressed next. 

1.	In Microsoft Visual Studio Code, open the inventory_aws_ec2.yaml file. As the first line, enter 3 hyphens to denote the beginning of the YAML file.

        ---

2.	In a web browser, navigate to the official Ansible documentation for using AWS EC2 as an inventory source: https://docs.ansible.com/ansible/latest/collections/amazon/aws/aws_ec2_inventory.html. Scroll down to the Examples section. Copy the circled code to the clipboard and minimize the web browser:
 
    ![1cfpacfdi](img/copy_files/prereq/ansible_config_files/dynamic_inventory/1.png) 

3.	As lines 2-4 of the inventory_aws_ec2.yaml file, paste the code from the clipboard. The plugin attribute indicates the EC2 inventory source will be used. As the EC2 servers created in section ii) reside in US-East-2, modify the region to us-east-2.

        plugin: amazon.aws.aws_ec2
        regions:
          - us-east-2

      Minimize Microsoft Visual Studio Code.

4.	Restore the web page containing the AWS EC2 inventory source documentation. In the Examples section, locate the comment that states, “Example using constructed features to create groups and set ansible_host.” Search for the keyed_groups specification. Copy the following code to the clipboard:
 
    ![2cfpacfdi](img/copy_files/prereq/ansible_config_files/dynamic_inventory/2.png) 

5.	Restore Microsoft Visual Studio Code. As lines 5-14 of the inventory_aws_ec2.yaml file, paste the code from the clipboard. Delete the comments and delete the prefix and key for architecture. These changes are summarized below:

        keyed_groups:
          - prefix: tag
            key: tags
          - prefix: instance_type
            key: instance_type

The dynamic inventory file is complete.

## b) Ansible Playbook <a name="playbook"></a>
The purpose of this section is to create an Ansible playbook.

1.	Restore the ansible-jenkins project in Microsoft Visual Studio Code. In the ansible folder, open the my-playbook.yaml file. 

2.	As the first line in the my-playbook.yaml file, add 3 hyphens to indicate the start of the Ansible playbook.

        ---

3.	This Ansible playbook will have a single play with several tasks. As the second line in my-playbook.yaml, name the play as “Install python3, docker, docker-compose, urllib3, and openssl.”

        - name: Install python3, docker, docker-compose, urllib3, and openssl
4.	Next, specify the hosts attribute with the value of all. This play will target all servers, which in this case, will be the EC2 servers.

        hosts: all

5.	To install software through yum and pip, root privileges will be required. Elevate permissions by using the become attribute and setting its value to true.

        become: true

    Additional reading about elevated privileges can be found here: https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_privilege_escalation.html 
6.	As a default behavior, Ansible will collect facts at the start of every play (https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_vars_facts.html). The Disabling facts section is shown below for convenience:

    ![1cfpacfp](img/copy_files/prereq/ansible_config_files/playbook/1.png)  

    For the sake of this project, disable this behavior by setting the gather_facts attribute to false:

        gather_facts: false

7.	Under the gather_facts attribute, add a tasks section. Name the first task as “Install python3, docker, and openssl.”

        tasks:
        - name: Install python3, docker, and openssl

8.	The first task will use Ansible’s built-in yum module found here: (https://docs.ansible.com/ansible/latest/collections/ansible/builtin/yum_module.html). The Synopsis section of the documentation states the yum module does not support Python 3:
 
    ![2cfpacfp](img/copy_files/prereq/ansible_config_files/playbook/2.png)

    Additionally, a brand new EC2 instance will come with Python 2 installed by default. What’s more, when the Ansible playbook is run, the Gathering Facts output will display the following warning regarding the Python version:

    ![3cfpacfp](img/copy_files/prereq/ansible_config_files/playbook/3.png) 

    To address this challenge of the Python version, reference the following Stack Overflow thread (https://stackoverflow.com/questions/58450608/how-can-i-specify-the-version-of-python-to-use-in-an-ansible-playbook). 

    The portion of the answer pertaining to specifying the Python version in the scope of a play is shown below:

    ![4cfpacfp](img/copy_files/prereq/ansible_config_files/playbook/4.png) 

    With this information in mind, at the same indentation level as name (but without the hyphen) set the ansible_python_interpreter variable to the Python2 location:

        vars:
          ansible_python_interpreter: /usr/bin/python

9.	From the previous step, the task now knows to use Python2 for the yum module. Referencing the first link from Step 8, at the same indentation level as the vars section, invoke the yum module and tell the play to install python3, docker, and openssl.

        ansible.builtin.yum:
          name:
            - python3
            - docker
            - openssl

10.	Update the yum repository’s list of packages by setting the update_cache attribute to yes:

        update_cache: yes
11.	As a last piece of the “Install python3, docker, and openssl” task, specify the state attribute as present to install the software indicated in Step 9.

        state: present

12.	Create a second task at the same indentation level as the “Install python3, docker, and openssl” task. Name the task “Install compatible version of urllib3, docker-compose, and Docker Python package.”

        - name: Install compatible version of urllib3, docker-compose, and Docker Python package

13.	Under the second task, use the built-in pip module to install urllib3, docker-compose, and docker. 

        ansible-builtin.pip:
          name: 
            - urllib3==1.26.7
            - docker-compose
            - docker

    For further reading on the built-in Ansible pip module, reference the official documentation here: https://docs.ansible.com/ansible/latest/collections/ansible/builtin/pip_module.html

14.	Create a final task and name it “Start Docker daemon.” Use Ansible’s built-in systemd module to start the docker service.

        - name: Start Docker daemon
          ansible.builtin.systemd:
            name: docker
            state: started

    More information about Ansible’s built-in systemd module can be found here: https://docs.ansible.com/ansible/latest/collections/ansible/builtin/systemd_service_module.html#ansible-collections-ansible-builtin-systemd-service-module 
15.	The final Ansible playbook (my-playbook.yaml) is summarized below:

        ---
        - name: Install python3, docker, docker-compose, urllib3, and openssl
          hosts: all
          become: true
          gather_facts: false
          tasks:
          - name: Install python3, docker, and openssl
            vars:
              ansible_python_interpreter: /usr/bin/python
            ansible.builtin.yum:
              name: 
                - python3
                - docker
                - openssl
              update_cache: yes
              state: present   
          - name: Install compatible version of urllib3, docker-compose, and Docker Python package
            ansible.builtin.pip:
              name:
                - urllib3==1.26.7
                - docker-compose
                - docker
          - name: Start Docker daemon
            ansible.builtin.systemd:
              name: docker
              state: started

The Ansible playbook is complete.
## iv) Jenkins Prerequisites: <a name="jprereq"></a>
Prior to writing the “copy files to ansible server” stage in Jenkinsfile, two Jenkins plugins and two credentials stored in Jenkins are required. The following sections will outline installing the SSH Agent plugin, as well as confirming the Credentials Binding Plugin exists. Then, the Ansible and EC2 server credentials will be added to Jenkins.

## a) Plugins: SSH Agent & Credentials Binding <a name="plugins1"></a>
1.	Login to the Jenkins management UI. In the left menu pane, click Manage Jenkins.
 
    ![1cfpjpp](img/copy_files/prereq/jenkins_prereq/plugins/1.png)

2.	On the Manage Jenkins screen, under System Configuration, click Plugins.

    ![2cfpjpp](img/copy_files/prereq/jenkins_prereq/plugins/2.png) 

3.	On the Plugins screen, select Available plugins from the left menu. Enter “ssh agent” in the search bar and press Enter. Click the checkmark next to SSH Agent and click the <b>Install</b> button. 

    <ins>Note</ins>: The Install dropdown shows an option to install after restart. As it is not necessary to reboot the Jenkins server after installation, just click the blue <b>Install</b> button.

    The SSH Agent plugin will permit Jenkins to connect to the Ansible server via the Ansible server’s SSH key. After establishing the connection, this plugin will also permit the use of secure copy (scp) commands to copy the ansible folder and prepare-ansible-server bash script to the Ansible server.
 
    ![3cfpjpp](img/copy_files/prereq/jenkins_prereq/plugins/3.png)

4.	The Download progress screen will appear and indicate a successful installation.

    ![4cfpjpp](img/copy_files/prereq/jenkins_prereq/plugins/4.png) 

5.	On the left menu pane, click Installed plugins.

    ![5cfpjpp](img/copy_files/prereq/jenkins_prereq/plugins/5.png) 

6.	On the Plugins screen, enter “Credentials Binding” in the search bar. Confirm the Credentials Binding plugin is enabled.

    ![6cfpjpp](img/copy_files/prereq/jenkins_prereq/plugins/6.png)
 
The Jenkins plugins are now ready to be used in the Jenkinsfile.

## b) Ansible Server Key Credentials <a name="ansible_server_key"></a>
This section will store the Ansible server key credentials inside the Jenkins credentials store.
1. In the Jenkins management UI, click Manage Jenkins from the left menu.

    ![1cfpjpaskc](img/copy_files/prereq/jenkins_prereq/ansible_server_key_creds/1.png)
 
2. On the Manage Jenkins screen, under the Security section, click Credentials.
 
    ![2cfpjpaskc](img/copy_files/prereq/jenkins_prereq/ansible_server_key_creds/2.png)

3. On the Credentials screen, under the Stores scoped to Jenkins section, click global.
    
    ![3cfpjpaskc](img/copy_files/prereq/jenkins_prereq/ansible_server_key_creds/3.png)

4. On the Global credentials (unrestricted) screen, click the <b>+ Add Credentials</b> button.

    ![4cfpjpaskc](img/copy_files/prereq/jenkins_prereq/ansible_server_key_creds/4.png) 

5. On the New credentials screen, click the Kind dropdown menu and select the “SSH Username with private key” option. Set the ID and description as ansible-server-key. Specify the username as root. In the Private Key section, select the radio button “Enter directly.”

    ![5cfpjpaskc](img/copy_files/prereq/jenkins_prereq/ansible_server_key_creds/5.png)
 
6. From the previous screenshot, in the Private Key section, click the blue <b>Add</b> button. A text box will now display.

    ![6cfpjpaskc](img/copy_files/prereq/jenkins_prereq/ansible_server_key_creds/6.png)
 
    Minimize the Jenkins management UI.

7. Open PowerShell and type the following command to display the local workstation’s SSH private key.

        type ~/.ssh/id_rsa

    Notice the header and footer are in the new OpenSSH format.

    ![7cfpjpaskc](img/copy_files/prereq/jenkins_prereq/ansible_server_key_creds/7.png) 

8. As Jenkins only understands the classic format (e.g. header and footer with RSA PRIVATE KEY), the private SSH key will need to be converted. Enter the following command in PowerShell to convert the private SSH to the classic format:

        ssh-keygen -p -f .ssh/id_rsa -m pem -P "" -N ""

    Where:<br>
    -f = path to the private key<br>
    -m = the file extension<br>
    -P = old passphrase<br>
    -N = new passphrase<br>

    When prompted for the new passphrase, leave it empty by pressing Enter. When prompted to reenter the passphrase, press Enter again. The following output will display:

    ![8cfpjpaskc](img/copy_files/prereq/jenkins_prereq/ansible_server_key_creds/8.png) 

9. Enter the following command to confirm the private SSH key has been converted to the classic format:

        type ~/.ssh/id_rsa
 
    ![9cfpjpaskc](img/copy_files/prereq/jenkins_prereq/ansible_server_key_creds/9.png)

10. Including the header and footer, copy the entire private SSH key to the clipboard. Close PowerShell and restore the Jenkins management UI. Paste the contents from the clipboard into the Key textbox. Click the <b>Create</b> button.
 
    ![10cfpjpaskc](img/copy_files/prereq/jenkins_prereq/ansible_server_key_creds/10.png)

11. Confirm the ansible-server-key is present on the Global credentials (unrestricted) page.

    ![11cfpjpaskc](img/copy_files/prereq/jenkins_prereq/ansible_server_key_creds/11.png) 

## c) EC2 Server Key Credentials <a name="ec2_server_key"></a>
This section will store the EC2 server key credentials inside the Jenkins credentials store.
1. In the Jenkins management UI, click Manage Jenkins from the left menu.

    ![1cfpjpeskc](img/copy_files/prereq/jenkins_prereq/ec2_server_key_creds/1.png)
 
2. On the Manage Jenkins screen, under the Security section, click Credentials.
 
    ![2cfpjpeskc](img/copy_files/prereq/jenkins_prereq/ec2_server_key_creds/2.png)

3. On the Credentials screen, under the Stores scoped to Jenkins section, click global.

    ![3cfpjpeskc](img/copy_files/prereq/jenkins_prereq/ec2_server_key_creds/3.png)
 
4. On the Global credentials (unrestricted) screen, click the <b>+ Add Credentials</b> button.
 
    ![4cfpjpeskc](img/copy_files/prereq/jenkins_prereq/ec2_server_key_creds/4.png)

5. On the New credentials screen, click the Kind dropdown menu and select the “SSH Username with private key” option. Set the ID and description as ec2-server-key. Specify the username as ec2-user. In the Private Key section, select the radio button “Enter directly.”

    ![5cfpjpeskc](img/copy_files/prereq/jenkins_prereq/ec2_server_key_creds/5.png)
 
    Minimize the Jenkins management UI.

6. Recall in Section II.A.ii, Step 8 of this project walkthrough the ansible-jenkins.pem file was saved to the Downloads folder. Open PowerShell and enter the following command to display the contents of ansible-jenkins.pem.

        type ~\Downloads\ansible-jenkins.pem

    Notice the output is in the classic format, so no conversion is necessary.

    ![6cfpjpeskc](img/copy_files/prereq/jenkins_prereq/ec2_server_key_creds/6.png) 

7. Including the RSA PRIVATE KEY header and footer, copy the contents of the .pem file to the clipboard. Close out of PowerShell.

8. Restore the Jenkins management UI and paste the contents from the clipboard inside the Key textbox. Click the <b>Create</b> button.

    ![7cfpjpeskc](img/copy_files/prereq/jenkins_prereq/ec2_server_key_creds/7.png)
 
9. On the Global credentials (unrestricted) screen, confirm the ec2-server-key credential is present.
 
    ![8cfpjpeskc](img/copy_files/prereq/jenkins_prereq/ec2_server_key_creds/8.png)

## B) Create Jenkinsfile <a name="jf1"></a>
With all prerequisites satisfied, the "copy files to ansible server" stage in Jenkinsfile can now be written.
1. Restore the ansible-jenkins project in Microsoft Visual Studio Code. At the root of the project folder, create a new file called Jenkinsfile.

    ![1cfpjpeskc](img/copy_files/jenkinsfile/1.png)
 
2. As the first line of the Jenkinsfile, add a shebang so the code editor recognizes the pipeline will be written in Groovy script.

        #!/usr/bin/env groovy

3. Enter a couple carriage returns for spacing. Use the pipeline block to declare the pipeline:

        pipeline {

        }

4. Inside the pipeline block, specify agent any to tell Jenkins to run the entire pipeline on any agent that is available.

        agent any

5. Under the <i>agent any</i> line, create a stages block:

        stages {

        }

6. Inside the stages block, create the first individual stage block named “copy files to ansible server.”

        stage("copy files to ansible server") {

        }

7. Inside the “copy files to ansible server” stage, nest the steps and script blocks as shown below:

        stage("copy files to ansible server") {
            steps {
                  script {
                  }
            }
        }

8. Confirm the Jenkinsfile up to this point looks like this:

        pipeline {
            agent any
            stages {
                stage("copy files to ansible server") {
                    steps {
                        script {
                        }
                    }
                }
            }
        }

9. Inside the script block, display output to the user to indicate all necessary files will be copied to the ansible control node:

        echo "copying all necessary files to ansible control node"

10. Recall the SSH Agent plugin and Ansible Server Key credentials are prerequisites to writing the “copy files to ansible server” stage in Jenkinsfile. Below the echo statement in the script block, create a sshagent block and pass in the Ansible Server Key.

        sshagent(['ansible-server-key']) {

        }

11. Inside the sshagent block, use Secure Copy to copy the entire contents of the project’s ansible folder to the root user’s home directory on the Ansible server. The -o flag stands for option and setting StrictHostKeyChecking to the value of no bypasses the yes/no prompt for adding to the list of known knowns. Specify the root user so that the Jenkins pipeline does not mistake a jenkins user existing on the Ansible server. 

        sh "scp -o StrictHostKeyChecking=no ansible/* root@178.128.156.253:/root"

12. Recall the Credentials Binding and EC2 Server Key are prerequisites to writing this stage in Jenkinsfile. Open a web brower and navigate to the following link: https://www.jenkins.io/doc/pipeline/steps/credentials-binding/. The below screenshot from this link outlines the sshUserPrivate credential type and options:

    ![2cfpjpeskc](img/copy_files/jenkinsfile/2.png) 

    Inside the sshagent block, enter a couple carriage returns after the first scp command. Add a withCredentials block and specify the sshUserPrivateKey credential type. Specify the credentialsId as the EC2 Server Key. Enter the keyFileVariable as ‘keyfile’ and the usernameVariable as ‘user’ as shown below:

        withCredentials([sshUserPrivateKey(credentialsId: 'ec2-server-key', keyFileVariable: 'keyfile', usernameVariable: 'user')]) {

        }

13. Inside the withCredentials block, use Secure Copy to copy the EC2 Server Key to the root user’s home directory on the Ansible server. The ${keyfile} variable is used to pass the contents of the key stored in Jenkins. 

    Recall the ansible.cfg in the ansible folder specifies the private_key_file option as ~/ssh-key.pem. Accordingly, use ssh-key.pem as the name of the saved file on the Ansible server’s root home folder.

        sh "scp ${keyfile} root@178.128.156.253:/root/ssh-key.pem"

14. Add, commit, and push changes to the GitLab repository.

        git add .
        git commit -m "Add Jenkinsfile with copy files stage"
        git push

The "copy files to ansible server" stage in Jenkinsfile is now complete.
## C) Jenkins Pipeline Creation/Execution <a name="pipeline1"></a>
With the "copy files to ansible server" stage completed in Jenkinsfile, it is now time to create the Jenkins pipeline in the Jenkins management UI and execute the pipeline.
1.	Login to the Jenkins management UI. On the Dashboard, click +New Item from the left menu.

    ![1cfp](img/copy_files/pipeline/1.png) 

2.	Specify the item name as “ansible-pipeline” and select the basic Pipeline option. Click <b>OK</b>.
 
    ![2cfp](img/copy_files/pipeline/2.png) 

3.	On the General screen, scroll down to the Pipeline section. In the Definition dropdown, select Pipeline script from SCM. In the SCM dropdown, select Git.

    ![3cfp](img/copy_files/pipeline/3.png)  

4.	Open the GitLab project in a new browser tab. Click the <b>Clone</b> dropdown button and copy the “Clone with HTTPS” address to the clipboard.
 
    ![4cfp](img/copy_files/pipeline/4.png) 

5.	Back on the General screen in the pipeline configuration, paste the contents from the clipboard to the Repository URL textbox. In the credentials dropdown, select the GitLab credentials.

    ![5cfp](img/copy_files/pipeline/5.png)  

    <ins>Note</ins>: This project walkthrough assumes that GitLab credentials are already added to Jenkins. If necessary, GitLab credentials can be added on the fly by clicking the Add button dropdown.

6.	In the Branches Specifier textbox of the Branches to build section, specify the branch as master. Click the <b>Save</b> button.

    ![6cfp](img/copy_files/pipeline/6.png)  

7.	On the Pipeline ansible-pipeline screen, click the Build Now link from the left menu.

    ![7cfp](img/copy_files/pipeline/7.png)  

8.	The pipeline execution is successful:

    ![8cfp](img/copy_files/pipeline/8.png) 
 
## D) Pipeline Validation & Security Optimization <a name="optimize1"></a>
This section will validate the “copy files to ansible server” stage copied the ansible folder and private EC2 SSH key to the server. This section will also provide an optimization to the Jenkinsfile.
1.	Open PowerShell and SSH into the Ansible server. Issue an <b>ls</b> command to confirm the ansible folder contents and the private EC2 SSH key exist:

    ![1cfvso](img/copy_files/validation_security_optimization/1.png)  

2.	Exit the SSH session.

        exit

3.	Inside the Jenkins management UI, navigate to the Dashboard. Click inside the ansible-pipeline job.
 
    ![2cfvso](img/copy_files/validation_security_optimization/2.png)  

4.	In the left pane, click on build #1.

    ![3cfvso](img/copy_files/validation_security_optimization/3.png)   

5.	Notice the security warning below:

    ![4cfvso](img/copy_files/validation_security_optimization/4.png)   

6.	In Jenkinsfile, the security issue from Step 5 pertains to the syntax used with the keyFileVariable (keyfile) in the withCredentials block. The use of double quotes and curvy brackets will mistakenly let the Groovy interpreter reveal the keyfile secret in the command line history.

    To fix this, open Microsoft Visual Studio Code and restore the Jenkinsfile in the ansible-jenkins project. Modify the scp command in the withCredentials block by using single quotes and removing the curvy brackets as shown below:

        sh 'scp $keyfile root@178.128.156.253:/root/ssh-key.pem'

7.	Open a terminal inside Microsoft Visual Studio code. Use the <b>git</b> command to add the changes, add a commit message, and push to the GitLab repository.

        git add .
        git commit -m "Fix security warning for .pem file"
        git push

8.	Return to the Jenkins management UI. On the Pipeline ansible-pipeline screen, click Build Now to rerun the Jenkins pipeline. The second build is successful:

    ![5cfvso](img/copy_files/validation_security_optimization/5.png)   

9.	On the left navigation menu, click build #2.

    ![6cfvso](img/copy_files/validation_security_optimization/6.png)   

10.	Click on Status in the left menu and confirm the security warning is resolved.
 
    ![7cfvso](img/copy_files/validation_security_optimization/7.png)  

## III. CI/CD Stage: Execute Ansible Playbook <a name="execute_playbook"></a>
Prior to writing the final stage of the Jenkins pipeline, the SSH Pipeline Steps Plugin must be installed in Jenkins. Once installed, the Jenkinsfile will be updated to add the “execute ansible playbook” stage. The pipeline will then be executed, validated, and optimized.

## A) Jenkins Prerequisite: SSH Pipeline Steps Plugin <a name="plugins2"></a>

The SSH Pipeline Steps plugin in Jenkins will allow the “execute ansible playbook” stage to run the <b>ansible-playbook</b> command. This section covers the installation of the SSH Pipeline Steps Plugin in Jenkins.
1. Login to the Jenkins management UI. Click Manage Jenkins in the left menu.

    ![1epp](img/execute_playbook/plugin/1.png)

2. On the Manage Jenkins screen, in the System Configuration section, click Plugins.
 
    ![2epp](img/execute_playbook/plugin/2.png)

3. On the Plugins screen, click Available Plugins from the left navigation pane. Enter “ssh pipeline steps” in the search bar. Select the checkbox to install the SSH Pipeline Steps plugin and click the <b>Install</b> button.

    ![3epp](img/execute_playbook/plugin/3.png) 

4. The Download progress screen will show the plugin installation is successful.

    ![4epp](img/execute_playbook/plugin/4.png) 

The SSH Pipeline Steps plugin is now ready to use in the Jenkinsfile.

## B) Jenkinsfile <a name="jf2"></a>
In this section, the "execute ansible playbook" stage will be written in Jenkinsfile.
1. Restore the ansible-jenkins project in Microsoft Visual Studio Code and open the Jenkinsfile. At the same indentation level as the “copy files to ansible server” stage, create a new individual stage block named “execute ansible playbook.”

        stage("execute ansible playbook") {

        }

2. In the “execute ansible playbook” stage block, create nested steps and script blocks as shown below:

        stage("execute ansible playbook") {
            steps {
               script {
               }
            }
        }

3. In the script block, display output to the user that the Ansible Playbook is being called to configure the EC2 instances:
        
        echo "calling ansible playbook to configure ec2 instances"

    Minimize Microsoft Visual Studio Code.

4.  Open the following webpage in a web browser: https://plugins.jenkins.io/ssh-steps/. Scroll down to the withCredentials example in the Examples section. Copy the following code to the clipboard.

    ![1epj](img/execute_playbook/jenkinsfile/1.png)
 
    Restore the Jenkinsfile in Microsoft Visual Studio Code. Under the echo command in the script block, paste the code from the clipboard with the following changes:<br><br>- Set the remote.name to “ansible-server”<br>- Set the remote.host to the IP of the Ansible server

    The changes are summarized as follows:

        def remote = [:]
        remote.name = "ansible-server"
        remote.host = "x.x.x.x"
        remote.allowAnyHosts = true

5. Enter a couple carriage returns. Still in the script block, use the withCredentials plugin again. For efficiency and to ensure there are no syntactical errors, copy the following line from the “copy files to ansible server” stage.

        withCredentials([sshUserPrivateKey(credentialsId: 'ec2-server-key', keyFileVariable: 'keyfile', usernameVariable: 'user')]) {
        
        }

    Change the credentialsId value to ‘ansible-server-key.’

6.  Refer back to the official Jenkins documentation on using the SSH Pipeline Steps plugin (https://plugins.jenkins.io/ssh-steps/). Notice the user and identityFile keys in the Remote section of Configuration.

    ![2epj](img/execute_playbook/jenkinsfile/2.png)
 
    Once again, scroll to the withCredentials example in the Examples section. Copy the circled keys and associated values to the clipboard.

    ![3epj](img/execute_playbook/jenkinsfile/3.png)
 
7. Inside the withCredentials block, paste the user and identityFile keys from the clipboard. Make the following adjustments:<br>- Change the user key’s value to user to match the usernameVariable in the withCredentials statement<br>- Change the identityFile key’s value to keyfile to match the keyFileVariable in the withCredentials statement. 

    These changes are summarized as follows:

        remote.user = user
        remote.identityFile = keyfile

8. Inside the withCredentials block and under the identityFile key, add the following line to run the <b>ls -l</b> command:

        sshCommand remote: remote, command: "ls -l"

    <ins>Note</ins>: The <b>ls -l</b> command is for testing purposes and will be changed later.

9. Open a terminal in Microsoft Visual Studio code. Use the git command to add the changes, specify a commit message, and push to the Gitlab repository.

        git add .
        git commit -m "Add remote execution"
        git push

## C) Jenkins Pipeline Execution <a name="pipeline_execute"></a>
This section reruns the Jenkins pipeline with the addition of the "execute ansible playbook" stage.
1. Return to the Jenkins management UI and rerun the ansible-pipeline job. Confirm the build is successful:

    ![1eppi](img/execute_playbook/pipeline/1.png) 

2. On the Pipeline ansible-pipeline screen, click build #3 on the left menu.

    ![2eppi](img/execute_playbook/pipeline/2.png) 

3. Click on Console Output on the left.

    ![3eppi](img/execute_playbook/pipeline/3.png) 

4. On the Console Output screen, scroll down and confirm the <b>ls -l</b> command is executed:

    ![4eppi](img/execute_playbook/pipeline/4.png) 

5. Restore the Jenkinsfile in the ansible-jenkins project. In the withCredentials block, modify the sshCommand by replacing the <b>ls -l</b> command with the <b>ansible-playbook</b> command. This command will run the Ansible playbook.

        sshCommand remote: remote, command: "ansible-playbook my-playbook.yaml"

6. In the terminal pane of Microsoft Visual Studio Code, use the <b>git</b> command to add the changes, specify a commit message, and push to the Gitlab repository.

        git add .
        git commit -m "Execute ansible command remotely"
        git push

7. Restore the Jenkins management UI and rerun the ansible-pipeline job. Confirm the build is successful:
 
    ![5eppi](img/execute_playbook/pipeline/5.png) 

8. On the Pipeline ansible-pipeline screen, click build #4 on the left menu.
 
    ![6eppi](img/execute_playbook/pipeline/6.png) 

9. Click on Console Output on the left.

    ![7eppi](img/execute_playbook/pipeline/7.png) 

10. On the Console Output screen, scroll down and confirm the Ansible playbook is run successfully:

    ![8eppi](img/execute_playbook/pipeline/8.png) 

## IV. Jenkins Pipeline Optimization <a name="optimize2"></a>
Two optimizations will be applied to the Jenkins pipeline. First, since the Ansible server IP appears three times in Jenkinsfile, the Ansible server IP will be converted to an environment variable. Converting to an environment variable will make it so the Ansible server IP address only has to be changed in one place moving forward. Second, a bash script will be used to automate the installation of Ansible and the Python-related packages for new server builds in the future.

## A) Environment Variable: Ansible Server IP <a name="env_var"></a>
In this section, the Ansible Server IP will be converted to an environment variable.
1. In Microsoft Visual Studio Code, restore the Jenkinsfile in the ansible-jenkins project. Notice the Ansible server IP appears three times – twice in the “copy files to ansible server” stage and once in the “execute ansible playbook” stage. 

    Create an environment block between the agent any and stages block as show below:

        pipeline {
            agent any
            environment {
            }
            stages {
            }
        }

2. Inside the environment block, create a variable called ANSIBLE_SERVER and assign the Ansible server IP address as the value.

        ANSIBLE_SERVER = "x.x.x.x"

3. In the “copy files to ansible server” stage, locate the sshagent block where the SSH Agent plugin is used. Replace the Ansible IP with ${ANSIBLE_SERVER} as shown here:

        sh "scp -o StrictHostKeyChecking=no ansible/* root@${ANSIBLE_SERVER}:/root"

4. In the “copy files to ansible server” stage, locate the withCredentials block where the Credentials Binding plugin is used. Replace the Ansible IP with $ANSIBLE_SERVER. Recall the curvy brackets and double quotes cannot be used here to prevent the Jenkins pipeline from showing the secret in the command history.

        sh 'scp $keyfile root@$ANSIBLE_SERVER:/root/ssh-key.pem'

5. In the “execute ansible playbook” stage, locate the script block. Replace the value of the remote.host key with the value of ANSIBLE_SERVER as shown here:

        remote.host = ANSIBLE_SERVER

6. In the terminal pane of Microsoft Visual Studio Code, use the <b>git</b> command to add the changes, specify a commit message, and push to the Gitlab repository.

        git add .
        git commit -m "Create ansible server env var"
        git push

7. Return to the Jenkins management UI and rerun the ansible-pipeline job. Confirm the build is successful:

    ![1oe](img/optimize/env_var/1.png)

## B) Bash Script: Prepare-Ansible-Server.sh <a name="bash"></a>
The purpose of this section is to automate sections II.A.i.b and II.A.i.c so that Ansible and Python's boto3 and botocore packages can be installed non-interactively. This is useful if installing on a fresh Ansible server in a Jenkins pipeline.

1.	Restore the ansible-jenkins project in Microsoft Visual Studio Code. At the root of the project folder, create a new bash script called prepare-ansible-server.sh.

    ![1obs](img/optimize/bash_script/1.png) 

2.	Add a shebang at the top of the prepare-ansible-server.sh file to notify the code editor this is the start of a bash script.

        #!/usr/bin/env bash

3.	Add the following <b>apt</b> commands to install Ansible, pip3, and boto3 and botocore Python packages.  The -y flag confirms the desire to proceed with the installation without manual intervention.

        #!/usr/bin/env bash

        apt update
        apt install ansible -y
        apt install python3-pip -y
        apt install python3-boto3
        apt install python3-botocore

4.	Open the Jenkinsfile. In the “execute ansible playbook” stage, locate the withCredentials block. Add the following line above sshCommand:

        sshScript remote: remote, script: "prepare-ansible-server.sh"

5.	In the terminal pane of Microsoft Visual Studio Code, use the <b>git</b> command to add the changes, specify a commit message, and push to the Gitlab repository.

        git add .
        git commit -m "Add Ansible server script optional step"
        git push

6.	Return to the Jenkins management UI. Rerun the ansible-pipeline job and confirm the build is successful.

    ![2obs](img/optimize/bash_script/2.png)  

7. Click on Build #6 in the left pane.

    ![3obs](img/optimize/bash_script/3.png)  

8. Click Console Output on the left pane.

    ![4obs](img/optimize/bash_script/4.png)  

9.	On the Console Output screen, review the console output. Notice the output shows that ansible, pip3, and the boto3 and botocore packages are already the latest versions.

    ![5obs](img/optimize/bash_script/5.png) 
 
## V. Project Teardown <a name="teardown"></a>
To not incur any additional charges in Digital Ocean and AWS, the server resources will be deleted from the respective cloud portals.
1.	Login to Digital Ocean. In the left navigation menu, click Manage > Droplets.

    ![1t](img/teardown/1.png) 
 
2.	Next to the ansible-server, click the <b>More</b> dropdown and click Destroy.

    ![2t](img/teardown/2.png)  

3.	Click the <b>Destroy this Droplet</b> button.

    ![3t](img/teardown/3.png)  

4.	In the Destroy Droplet dialog box, enter the name of the droplet to confirm deletion and click the <b>Destroy</b> button.

    ![4t](img/teardown/4.png)  

5.	The ansible-server droplet will no longer display on the Droplets page. Repeat steps 2-4 for the Jenkins server.

6.	Login to the AWS management portal. Navigate to the EC2 Dashboard. Click on Instances (running).
 
    ![5t](img/teardown/5.png) 

7.	Select the checkboxes for both t2.micro instances.

    ![6t](img/teardown/6.png)  

8.	In the Actions dropdown, click Manage instance state.

    ![7t](img/teardown/7.png)  

9.	On the Manage instance state screen, select the Terminate radio button. Click the <b>Change state</b> button.
 
    ![8t](img/teardown/8.png) 

10.	On the Terminate instances dialog box, click the <b>Terminate</b> button.

    ![9t](img/teardown/9.png)  

11.	The instance state will change to “Shutting down.”

    ![10t](img/teardown/10.png)  

12.	After a moment, confirm the instances are in the Terminated instance state.

    ![11t](img/teardown/11.png)  

With cleanup complete, there will be no additional charges from Digital Ocean and AWS.
